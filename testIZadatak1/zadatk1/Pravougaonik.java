package zadatk1;

public class Pravougaonik extends Povrs implements Obim,Povrsina,Opisivanje{
	private Double a;
	private Double b;
    private  Tacka centar;
	

	public Pravougaonik() {
		super();
		
	}



	public Pravougaonik(Double a, Double b,Tacka centar) {
		super(centar);
		this.a = a;
		this.b = b;
		this.centar=centar;
	}



	@Override
	public Double povrsina() {
		Double popravog = this.a*this.b;
		return popravog;
	}



	@Override
	public Double obim() {
		Double obimpravoug=2*(this.a+this.b);
		return obimpravoug;
	}
	private Double dijagonalaprav()
	{
		Double rast=Math.sqrt(Math.pow((this.a),2)+Math.pow((this.b),2));
		return rast;
	}



	@Override
	public Krug opis() {
	Krug krug=new Krug(dijagonalaprav()/2,this.centar);
	   	
	    
		return krug;
	}



	@Override
	public String toString() {
		return "Pravougaonik [a=" + a + ", b=" + b + ", centar=" + centar + "]";
	}

}
