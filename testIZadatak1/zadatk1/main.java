package zadatk1;

import java.util.ArrayList;
import java.util.List;

public class main {

	public static <E> void main(String[] args) {
		Tacka t1 = new Tacka(22.22, 33.33);
		Tacka t2 = new Tacka(5.22, 6.33);
        Tacka t3 = new Tacka(17.00,28.00);
		Kvadrat kvadrat = new Kvadrat(20.22, t1);
		Krug krug = new Krug(30.22, t2);
		Pravougaonik pravougaonik = new Pravougaonik(20.00,10.00,t1);
		
		List<Object> povrs=new ArrayList<>();
		povrs.add(krug);
		povrs.add(kvadrat);
		povrs.add(pravougaonik);
		 Double g = ((Krug)povrs.get(0)).rastojanjeCentara((Kvadrat)povrs.get(1));
		
		Double g1 = ((Krug) povrs.get(0)).povrsina();
		Double g2 =((Krug)povrs.get(0)).rastojanjeCentara(((Krug)povrs.get(0)).opis());
		Double g3 = ((Pravougaonik)povrs.get(2)).opis().obim();
		System.out.println(" rastojanje centara krug kvadrat   "+g
		+"\npovrs kruga   "+g1
		+" \nrastojanje centara kruga i opisanog kvadrata  "+g2
		+" \n obim opisanog kruga oko pravougaonika  "+g3);

	}

}
