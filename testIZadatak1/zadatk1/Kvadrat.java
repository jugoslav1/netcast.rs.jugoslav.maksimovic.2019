package zadatk1;

public class Kvadrat extends Povrs implements Obim, Povrsina, Opisivanje {
	private Double a;
	Tacka centar;
	public Kvadrat(Double a, Tacka centar) {
		super(centar);
		this.a = a;
        this.centar=centar;
	}

	public Kvadrat() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public Double povrsina() {
		Double pokvad = this.a * this.a;
		return pokvad;
	}

	@Override
	public Double obim() {
		Double obimkvadrata = 2 * this.a;
		return obimkvadrata;
	}

	private Double dijagonalakvadrata() {
		Double rast = Math.sqrt(Math.pow((this.a), 2) + Math.pow((this.a), 2));
		return rast;
	}

	public Double getA() {
		return a;
	}

	public void setA(Double a) {
		this.a = a;
	}

	@Override
	public Krug opis() {
		Krug krug = new Krug(dijagonalakvadrata() / 2,this.centar);
		
		return krug;
	}

	@Override
	public String toString() {
		return "Kvadrat [a=" + a + ", centar=" + centar + "]";
	}

	



}
