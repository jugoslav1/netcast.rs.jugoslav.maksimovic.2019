package zadatk1;

public class Krug extends Povrs implements Obim, Povrsina, Opisivanje {
	private Double r;
	Tacka centar;

	public Krug() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Krug(Double r, Tacka centar) {
		super(centar);
		this.r = r;
        this.centar=centar;
	}

	public Double getR() {
		return r;
	}

	;

	public void setR(Double r) {
		this.r = r;
	}

	@Override
	public Double povrsina() {
		Double pokrug = 2 * this.r * this.r * 3.14;
		return pokrug;
	}

	@Override
	public Double obim() {
		Double obkrug = 2 * this.r * 3.14;
		return obkrug;
	}

	@Override
	public Kvadrat opis() {
		Kvadrat kvadrat = new Kvadrat(2 * this.r,this.centar);
		kvadrat.setA(2 * this.r);
		return kvadrat;
	}

	@Override
	public String toString() {
		return "Krug [r=" + r + ", centar=" + centar + "]";
	}

	

}
