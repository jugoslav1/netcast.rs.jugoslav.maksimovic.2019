package zadatk1;

public class Tacka {
private Double x;
private Double y;
public Tacka(Double x, Double y) {
	super();
	this.x = x;
	this.y = y;
}
public Tacka() {
	
}


public Double getX() {
	return x;
}
public void setX(Double x) {
	this.x = x;
}
public Double getY() {
	return y;
}
public void setY(Double y) {
	this.y = y;
}


public Double rastojanje(Tacka t)
{
	Double rast=Math.sqrt(Math.pow((Math.abs(this.x-t.getX())),2)+Math.pow((Math.abs(this.y-t.getY())),2));
	return rast;
}
@Override
public String toString() {
	return "Tacka [x=" + x + ", y=" + y + "]";
}


}
