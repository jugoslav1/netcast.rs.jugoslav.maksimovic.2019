package com.example.demo.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.Test;

@Repository
public interface TestDao extends CrudRepository<Test, Integer>{

	static Object notFound() {
		// TODO Auto-generated method stub
		return null;
	}

	

}
