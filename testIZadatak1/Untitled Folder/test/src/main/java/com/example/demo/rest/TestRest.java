package com.example.demo.rest;




import java.util.List;

import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.TestDao;
import com.example.demo.domain.Test;
import com.example.demo.servis.TestServis;



@RestController
@RequestMapping("/test")
public class TestRest {
	

	@Autowired
	private TestServis testServis;
	
    //unesi objekat radi
    @PostMapping("/unos")
    public Test unesiTestera(@Valid @RequestBody Test ts)
    {
    	return testServis.unesisve(ts);
    }
    //izmeni objekat radi
    @PostMapping("/promena")
    public Test promeniTestera(@Valid @RequestBody Test ts)
    {
    	return testServis.unesisve(ts);
    }
    
    @DeleteMapping("/brisanje/{id}")
    public String brisi(@PathVariable(value="id") Integer id) 
    {
     testServis.obrisi(id);
	    return "obrisano";	}
    
    
    //nadji po id
  @GetMapping("/nadji/{id}")
    public Object pronadjiId(@PathVariable(value="id") Integer id)
    {
	  Object test=testServis.nadjijedan(id);
	  if(test==null) {
    	return null;
	  }else
	  return testServis.nadjijedan(id);
    }
    
    //izlistaj sve unose radi
    @GetMapping("/nadjisve")
    public List<Test> pronadjiSve1()
    {
    	return testServis.pronadjiSve1();
    }
}
