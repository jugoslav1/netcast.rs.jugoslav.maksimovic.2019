package com.example.demo.servis;




import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.TestDao;
import com.example.demo.domain.Test;

@Service
public class TestServis {

	@Autowired
	private TestDao testDao;
	//unesi ceo objekat radi
	@Transactional
	public Test unesisve(Test ts )
	{
		return testDao.save(ts);
	}
	//izmeni ceo objekat radi
	@Transactional
	public Test promeni(Test ts )
	{
		return testDao.save(ts);
	}
	//obrisi ceo objekat radi
	@Transactional
	public void obrisi(Integer tid )
	{
		 
		 testDao.deleteById(tid);
		 
	}
	//izlistaj sve objekte radi
	@Transactional
	public List<Test> pronadjiSve1()
	{
		return  (List<Test>) testDao.findAll();
	}
	
	// unesi objekt po id radi
	
	public Object  nadjijedan(Integer id)
	{
		Object test=testDao.findById(id);
		  if(test==null) {
		    	return null;}else
		return testDao.findById(id);
	}
	
	}
