package com.example.demo.scheduler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ClanDao;
import com.example.demo.dao.TerminDao;
import com.example.demo.domain.Clan;
import com.example.demo.domain.Termin;
import com.example.demo.domain.dto.ClanDugDto;
import com.example.demo.domain.dto.MailDto;
import com.example.demo.domain.dto.TerminVratiOdDoDto;
import com.example.demo.servis.ClanServis;

@Service
public class EmailServis implements EmailSendIF {
	@Autowired
	private JavaMailSender javaMailSender;
	@Autowired
	private ClanDao clanDao;
	@Autowired
	private TerminDao terminDao;
	private MailDto mailDto;

	public EmailServis(JavaMailSender javaMailSender, ClanDao clanDao, TerminDao terminDao, ClanServis clanServis) {
		super();
		this.javaMailSender = javaMailSender;
		this.clanDao = clanDao;
		this.terminDao = terminDao;

	}

	public EmailServis() {

	}

	@Override
	public void posaljiEmail() throws MailException {

		List<Clan> clanovi = (List<Clan>) clanDao.findAll();
		for (int i = 0; i < clanovi.size(); i++) {
			SimpleMailMessage mail = new SimpleMailMessage();
			mail.setTo(clanovi.get(i).getEmail());
			mail.setFrom("92f7f5b25e23da.mailtrap.io");
			mail.setSubject("mesecni izvestaj");
			MailDto mailDto = nadjiPoClanuTermineOdDoIDugovanje(clanovi.get(i).getId());
			String poruka;
			poruka = mailDto.toString();
			for (int g = 0; g < mailDto.getTermini().size(); g++) {
				poruka = "\n" + poruka + "\n\n\"" + mailDto.getTermini().get(g).toString();
			}
			mail.setText(poruka);
			javaMailSender.send(mail);
		}
	}

	public MailDto nadjiPoClanuTermineOdDoIDugovanje(Integer id) {
		Double dugovanje = 0.00;
		Double mesecnapretplata = 1500.00;
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DATE, 1);
		calendar.set(Calendar.MONTH,Calendar.MONTH-1);
		System.out.println("vreme "+calendar.getTime());
		Date pocetak = calendar.getTime();
		Date kraj = new Date();
		System.out.println("vreme "+pocetak+"   "+kraj);
		List<TerminVratiOdDoDto> terminVratiOdDoDtoNiz = new ArrayList<>();
		List<Termin> termini = terminDao.findByPocetakBetween(pocetak, kraj);
		Optional<Clan> clanId = clanDao.findById(id);
		if (clanId.isPresent()) {
			Clan clan = clanId.get();
			MailDto mailDto = new MailDto();
			TerminVratiOdDoDto terminVratiOdDoDto1 = new TerminVratiOdDoDto();
			for (int i = 0; i < termini.size(); i++) {
				System.out.println("termini "+termini.get(i+1).toString());
				if (termini.get(i).getClan().getEmail().equals(clan.getEmail())) {
					dugovanje = dugovanje + termini.get(i).getCena();

					terminVratiOdDoDto1.setId(termini.get(i).getId());
					terminVratiOdDoDto1.setImeClan(termini.get(i).getClan().getIme());
					terminVratiOdDoDto1.setImeTrener(termini.get(i).getTrener().getIme());
					terminVratiOdDoDto1.setCena(termini.get(i).getCena());
					terminVratiOdDoDto1.setPocetak(termini.get(i).getPocetak());
					terminVratiOdDoDto1.setKraj(termini.get(i).getKraj());
					terminVratiOdDoDtoNiz.add(terminVratiOdDoDto1);
				}
			}
			if (clan.getPretplata().getId().equals(1))
				dugovanje = dugovanje + mesecnapretplata;

			mailDto.setIme(clan.getIme());
			mailDto.setId(clan.getId());
			mailDto.setPrezime(clan.getPrezime());
			mailDto.setDugovanje(dugovanje);
			mailDto.setTermini(terminVratiOdDoDtoNiz);
			return mailDto;

		}
		return null;

	}
}
