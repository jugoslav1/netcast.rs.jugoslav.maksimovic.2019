package com.example.demo.scheduler;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class PlanerServis {

	@Autowired
	EmailSendIF eSendIF;

	@Scheduled( fixedDelay=10000/*cron="0 0 0 1 1/1 ? *"*/)
	public void send() {

		eSendIF.posaljiEmail();

	}
}
