
package com.example.demo.servis;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ClanDao;
import com.example.demo.dao.OvlasceniDao;
import com.example.demo.dao.TerminDao;
import com.example.demo.dao.TrenerDao;
import com.example.demo.domain.Clan;
import com.example.demo.domain.Ovlasceni;
import com.example.demo.domain.Pretplata;
import com.example.demo.domain.Termin;
import com.example.demo.domain.Trener;
import com.example.demo.domain.dto.ClanUnosDto;
import com.example.demo.domain.dto.TerminOdDoDto;
import com.example.demo.domain.dto.TerminUnosDto;
import com.example.demo.domain.dto.TerminVratiOdDoDto;
import com.example.demo.domain.dto.TrenerDto;
import com.example.demo.servis.intF.ClanServIF;
import com.example.demo.servis.intF.TerminServIF;
import com.example.demo.servis.intF.TrenerServIF;

@Service
public class TerminServis implements TerminServIF {
	@Autowired
	private TerminDao terminDao;
	@Autowired
	private ClanDao clanDao;
	@Autowired
	private TrenerDao trenerDao;
	@Autowired
	private OvlasceniDao ovlasceniDao;

	public TerminServis(TerminDao terminDao, ClanDao clanDao, TrenerDao trenerDao) {
		super();
		this.terminDao = terminDao;
		this.clanDao = clanDao;
		this.trenerDao = trenerDao;
	}

	@Override
	public Termin unesiTer(TerminUnosDto ted, String email) {

		Optional<Trener> trenerIdCena = trenerDao.findById(ted.getTrenerId());

		if (trenerIdCena.isPresent()) {
			int indexCeneTrenera = (int) (trenerIdCena.get().getTermini().size() / 10);
			Double indexCeneTreneraDouble = (double) indexCeneTrenera * 0.10;
			trenerIdCena.get()
					.setCena(trenerIdCena.get().getCena() + trenerIdCena.get().getCena() * indexCeneTreneraDouble);
			trenerDao.save(trenerIdCena.get());
		}

		Termin termin = new Termin();
		Ovlasceni ovlascen = ovlasceniDao.findByEmail(email);
		Optional<Clan> clanId = clanDao.findById(ted.getClanId());
		Optional<Trener> trenerId = trenerDao.findById(ted.getTrenerId());
		Double popust = 1.00;
		if (clanId.get().getGrupa().getPopust() != null)
			popust = 1 - clanId.get().getGrupa().getPopust() / 100;
		if (clanId.isPresent() && trenerId.isPresent()) {

			termin.setId(ted.getId());
			termin.setPocetak(ted.getPocetak());
			termin.setKraj(ted.getKraj());
			termin.setClan(clanId.get());
			termin.setTrener(trenerId.get());
			termin.setCena((clanId.get().getPretplata().getCena() + trenerId.get().getCena()) * popust);
			termin.setOvlasceni(ovlascen);
			return terminDao.save(termin);

		}
		return null;

	}

	@Override
	public List<Termin> pocetakOdDo1(TerminOdDoDto to) {
		return terminDao.findByPocetakBetween(to.getOd(), to.getDok());
	}

	@Override
	public List<TerminVratiOdDoDto> pocetakOdDo(TerminOdDoDto to)

	{
		List<Termin> terminNiz = new ArrayList();
		List<TerminVratiOdDoDto> terminVratiOdDoDtoNiz = new ArrayList();
		terminNiz = terminDao.findByPocetakBetween(to.getOd(), to.getDok());
		if (!terminNiz.isEmpty()) {
			for (int i = 0; i < terminNiz.size(); i++)

			{
				TerminVratiOdDoDto terminVratiOdDoDto = new TerminVratiOdDoDto();

				terminVratiOdDoDto.setId(terminNiz.get(i).getId());
				terminVratiOdDoDto.setCena(terminNiz.get(i).getCena());
				terminVratiOdDoDto.setPocetak(terminNiz.get(i).getPocetak());
				terminVratiOdDoDto.setKraj(terminNiz.get(i).getKraj());
				terminVratiOdDoDto.setImeClan(terminNiz.get(i).getClan().getIme());
				terminVratiOdDoDto.setImeTrener(terminNiz.get(i).getTrener().getIme());
				/*
				 * TerminVratiOdDoDto terminVratiOdDoDto = new
				 * TerminVratiOdDoDto(terminNiz.get(i).getId(),
				 * terminNiz.get(i).getClan().getIme(),terminNiz.get(i).getTrener().getIme(),
				 * terminNiz.get(i).getPocetak(), terminNiz.get(i).getKraj(),
				 * terminNiz.get(i).getCena());
				 */
				terminVratiOdDoDtoNiz.add(terminVratiOdDoDto);
			}

			return terminVratiOdDoDtoNiz;
		}
		return terminVratiOdDoDtoNiz;
	}

	@Override
	public String toString() {
		return "TerminServis [terminDao=" + terminDao + ", clanDao=" + clanDao + ", trenerDao=" + trenerDao + "]";
	}

}
