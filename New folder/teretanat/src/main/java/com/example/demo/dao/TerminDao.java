package com.example.demo.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.Clan;
import com.example.demo.domain.Termin;


@Repository
public interface TerminDao extends CrudRepository<Termin, Integer> {
	List<Termin> findByPocetakBetween(Date od, Date dok);
	List<Termin> findByClan(Clan clan);
	List<Termin> findByClanAndPocetakBetween(Clan clan,Date od, Date dok);
}
