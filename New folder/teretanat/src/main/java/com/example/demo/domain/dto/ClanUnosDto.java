package com.example.demo.domain.dto;

public class ClanUnosDto {

	private Integer id;
	private String ime;
	private String prezime;
	private String email;
	private Integer pretplataId;
	private Integer grupaId;

	public ClanUnosDto(Integer id, String ime, String prezime, String email, Integer pretplataId, Integer grupaId) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.email = email;
		this.pretplataId = pretplataId;
		this.grupaId = grupaId;
	}

	public Integer getGrupaId() {
		return grupaId;
	}

	public void setGrupaId(Integer grupaId) {
		this.grupaId = grupaId;
	}

	public ClanUnosDto() {

	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getPretplataId() {
		return pretplataId;
	}

	public void setPretplataId(Integer pretplataId) {
		this.pretplataId = pretplataId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getIme() {
		return ime;
	}

	public ClanUnosDto(Integer id, String ime, String prezime, String email, Integer pretplataId) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.email = email;
		this.pretplataId = pretplataId;
	}

}
