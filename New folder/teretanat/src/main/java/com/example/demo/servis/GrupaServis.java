package com.example.demo.servis;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.GrupaDao;
import com.example.demo.domain.Clan;
import com.example.demo.domain.Grupa;
import com.example.demo.domain.Pretplata;
import com.example.demo.servis.intF.GrupaIF;

@Service
public class GrupaServis implements GrupaIF {
	@Autowired
	private GrupaDao grupaDao;

	@Override
	public Grupa nadjiPoIdGrupa(Integer id) {

		Optional<Grupa> grupaId = grupaDao.findById(id);
		if (grupaId.isPresent()) {

			return grupaId.get();
		}
		return null;
	}

	@Override
	public Grupa unesiGrupa(Grupa grupa) {

		return grupaDao.save(grupa);
	}

}
