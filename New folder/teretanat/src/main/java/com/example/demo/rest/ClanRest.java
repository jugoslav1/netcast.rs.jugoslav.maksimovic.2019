package com.example.demo.rest;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Clan;
import com.example.demo.domain.dto.ClanDugDto;
import com.example.demo.domain.dto.ClanUnosDto;

import com.example.demo.servis.ClanServis;
import com.example.demo.servis.intF.ClanServIF;

@RestController
@RequestMapping("/clanovi")
public class ClanRest {
	@Autowired
	ClanServIF clanServIF;

	@PostMapping("/registracija")
	public Clan unesiClana(@RequestBody ClanUnosDto cld) {

		return clanServIF.unesiCl(cld);

	}

	@GetMapping("/poid/{id}")
	public Clan poId(@PathVariable Integer id) {
		return clanServIF.nadjiPoIdClan(id);

	}

	@PostMapping("/promena")
	public Clan promeniClana(@RequestBody ClanUnosDto cld) {

		return clanServIF.promeniClan(cld);
	}

	@GetMapping("/listaSvih")
	public List<Clan> sviClanovi() {
		return clanServIF.izlistajClanove();
	}

	@GetMapping("/poemail/{email}")
	public Clan poEm(@PathVariable String em) {
		return clanServIF.nadjiPoEmail(em);
	}

	@GetMapping("/tekucaDugovanjaClana/{id}")
	public ClanDugDto tekucaDugovanja(@PathVariable Integer id) {
		return clanServIF.tekucaDugovanja(id);
	}

}
