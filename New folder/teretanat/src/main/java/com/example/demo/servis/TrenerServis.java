package com.example.demo.servis;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.TrenerDao;
import com.example.demo.domain.Clan;
import com.example.demo.domain.Trener;
import com.example.demo.domain.dto.TrenerDto;
import com.example.demo.servis.intF.TrenerServIF;

@Service
public class TrenerServis implements TrenerServIF {

	private TrenerDao trenerDao;

	@Autowired
	public TrenerServis(TrenerDao trenerDao) {
		super();
		this.trenerDao = trenerDao;
	}

	@Override
	public Trener unesiTrenera(Trener tr) {

		return trenerDao.save(tr);
	}

	@Override
	public TrenerDto unesiNazadDto(Trener tr) {
		TrenerDto trenerDto = new TrenerDto();
		trenerDto.setCena(tr.getCena());
		trenerDto.setIme(tr.getIme());
		trenerDao.save(tr);
		return trenerDto;
	}

	@Override
	public List<Trener> izlistajTrenere() {

		return (List<Trener>) trenerDao.findAll();

	}

	@Override
	public Trener nadjiPoIdTrenera(Integer id) {

		Optional<Trener> trenerId = trenerDao.findById(id);
		if (trenerId.isPresent()) {

			int indexCeneTrenera = (int) (trenerId.get().getTermini().size() / 10);
			Double indexCeneTreneraDouble = (double) indexCeneTrenera * 0.10;
			trenerId.get().setCena(trenerId.get().getCena() + trenerId.get().getCena() * indexCeneTreneraDouble);
			return trenerId.get();
		}
		return null;

	}

	@Override
	public String toString() {
		return "TrenerServis [trenerDao=" + trenerDao + "]";
	}

}
