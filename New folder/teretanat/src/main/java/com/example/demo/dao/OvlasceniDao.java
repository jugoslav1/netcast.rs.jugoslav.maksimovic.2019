package com.example.demo.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.Ovlasceni;

@Repository
public interface OvlasceniDao extends CrudRepository<Ovlasceni, Integer> {
	Ovlasceni findByEmail(String email);

	Ovlasceni findByEmailAndLozinka(String email, String lozinka);
}
