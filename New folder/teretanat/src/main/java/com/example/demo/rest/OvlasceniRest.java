package com.example.demo.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Ovlasceni;
import com.example.demo.domain.dto.OvlasceniLogDto;
import com.example.demo.servis.intF.OvlasIF;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/ovlasceni")
public class OvlasceniRest {
	@Autowired
	private OvlasIF ovlasIF;

	@PostMapping("/logovanje")
	public String napraviJwt(@RequestBody OvlasceniLogDto old) {
		return ovlasIF.napraviToken(old);
	}

	@PostMapping("/registracija")
	public Ovlasceni kreirajOvlascenog(@RequestBody Ovlasceni ovl) {
		return ovlasIF.unesiOvl(ovl);
	}

	@GetMapping("/listaSvih")
	public List<Ovlasceni> listajOvlasceni() {
		return ovlasIF.izlistajOvlascene();
	}
}
