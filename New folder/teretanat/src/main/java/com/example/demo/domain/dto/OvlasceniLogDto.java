package com.example.demo.domain.dto;

import javax.validation.constraints.NotBlank;

public class OvlasceniLogDto {
	@NotBlank
	private String lozinka;
	@NotBlank
	private String email;

	public OvlasceniLogDto(String lozinka, String email) {
		super();
		this.lozinka = lozinka;
		this.email = email;
	}

	public OvlasceniLogDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "OvlasceniUnosDto [lozinka=" + lozinka + ", email=" + email + "]";
	}

}
