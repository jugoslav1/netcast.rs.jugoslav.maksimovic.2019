package com.example.demo.servis.intF;

import java.util.List;

import com.example.demo.domain.Clan;
import com.example.demo.domain.Pretplata;
import com.example.demo.domain.dto.ClanDugDto;
import com.example.demo.domain.dto.ClanUnosDto;

public interface ClanServIF {
	public Clan unesiCl(ClanUnosDto cld);

	public List<Clan> izlistajClanove();

	public Clan nadjiPoIdClan(Integer clid);

	public Clan nadjiPoEmail(String em);

	public Clan promeniClan(ClanUnosDto cld);

	public ClanDugDto tekucaDugovanja(Integer id);
}
