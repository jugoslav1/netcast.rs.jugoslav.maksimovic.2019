package com.example.demo.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Clan;
import com.example.demo.domain.Trener;
import com.example.demo.domain.dto.TrenerDto;
import com.example.demo.servis.TrenerServis;
import com.example.demo.servis.intF.TrenerServIF;

@RestController
@RequestMapping("/trener")
public class TrenerRest {

	private TrenerServIF trenerServIF;

	@Autowired
	public TrenerRest(TrenerServIF trenerServIF) {
		super();
		this.trenerServIF = trenerServIF;
	}

	@PostMapping("/registracija")
	public Trener unesiTrenera(@Valid @RequestBody Trener tr) {
		return trenerServIF.unesiTrenera(tr);
	}

	@PostMapping("/registracija1")
	public TrenerDto unesiTrenera1(@Valid @RequestBody Trener tr) {
		return trenerServIF.unesiNazadDto(tr);
	}

	@GetMapping("/listasvih")
	public List<Trener> sviTreneri() {
		return trenerServIF.izlistajTrenere();
	}

	@GetMapping("/poid/{id}")
	public Trener poId(@PathVariable Integer id) {
		return trenerServIF.nadjiPoIdTrenera(id);
	}
}
