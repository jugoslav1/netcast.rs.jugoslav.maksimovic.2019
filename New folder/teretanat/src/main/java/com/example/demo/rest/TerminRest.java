package com.example.demo.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Termin;
import com.example.demo.domain.dto.TerminOdDoDto;
import com.example.demo.domain.dto.TerminUnosDto;
import com.example.demo.domain.dto.TerminVratiOdDoDto;
import com.example.demo.servis.TerminServis;
import com.example.demo.servis.intF.TerminServIF;

@RestController
@RequestMapping("/termin")
public class TerminRest {

	@Autowired
	private TerminServIF terminServIF;

	@PostMapping("/registracija")
	public Termin unesiTermin(@Valid @RequestBody TerminUnosDto ted, HttpServletRequest httpr) {
		String email = (String) httpr.getAttribute("OVLASCENI_MAIL");
		return terminServIF.unesiTer(ted, email);
	}

	@GetMapping("/odDo")
	public List<TerminVratiOdDoDto> odDo(@RequestBody TerminOdDoDto to) {

		return terminServIF.pocetakOdDo(to);
	}

	@GetMapping("/odDo1")
	public List<Termin> odDo1(@Valid @RequestBody TerminOdDoDto to) {
		return terminServIF.pocetakOdDo1(to);
	}
}
