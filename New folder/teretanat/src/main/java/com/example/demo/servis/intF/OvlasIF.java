package com.example.demo.servis.intF;

import java.util.List;
import java.util.Optional;

import com.example.demo.domain.Ovlasceni;
import com.example.demo.domain.dto.OvlasceniLogDto;

public interface OvlasIF {

	public Ovlasceni findByEmail(String email);

	public Ovlasceni findByEmailAndLozinka(String email, String lozinka);

	public Ovlasceni unesiOvl(Ovlasceni ovl);

	public String napraviToken(OvlasceniLogDto old);

	public List<Ovlasceni> izlistajOvlascene();
}
