package com.example.demo.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.Grupa;

@Repository
public interface GrupaDao extends CrudRepository<Grupa, Integer> {

}
