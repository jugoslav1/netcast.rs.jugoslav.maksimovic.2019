package com.example.demo.domain.dto;

import java.util.Date;

import com.example.demo.domain.Trener;

public class TerminOdDoDto {

	private Date od;
	private Date dok;

	public TerminOdDoDto(Date od, Date dok) {
		super();
		this.od = od;
		this.dok = dok;
	}

	public TerminOdDoDto() {

	}

	public Date getOd() {
		return od;
	}

	public void setOd(Date od) {
		this.od = od;
	}

	public Date getDok() {
		return dok;
	}

	public void setDok(Date dok) {
		this.dok = dok;
	}

	@Override
	public String toString() {
		return "TerminOdDoDto [od=" + od + ", dok=" + dok + "]";
	}

}
