package com.example.demo.servis.intF;

import com.example.demo.domain.Grupa;

public interface GrupaIF {
	public Grupa unesiGrupa(Grupa grupa);

	public Grupa nadjiPoIdGrupa(Integer id);
}
