package com.example.demo.servis;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ClanDao;
import com.example.demo.dao.PretplataDao;
import com.example.demo.dao.TerminDao;
import com.example.demo.domain.Clan;
import com.example.demo.domain.Pretplata;
import com.example.demo.domain.Termin;
import com.example.demo.domain.dto.ClanDugDto;
import com.example.demo.domain.dto.ClanUnosDto;
import com.example.demo.servis.intF.ClanServIF;

@Service
public class ClanServis implements ClanServIF {

	private PretplataDao pretplataDao;
	private ClanDao clanDao;
	private TerminDao terminDao;

	@Autowired

	public ClanServis(PretplataDao pretplataDao, ClanDao clanDao, TerminDao terminDao) {
		super();
		this.pretplataDao = pretplataDao;
		this.clanDao = clanDao;
		this.terminDao = terminDao;
	}

	@Override
	public Clan nadjiPoEmail(String em) {

		Optional<Clan> clanId = clanDao.findByEmail(em);
		if (clanId.isPresent()) {

			return clanDao.findByEmail(em).get();
		}
		return null;

	}

	@Override
	public Clan nadjiPoIdClan(Integer clid) {

		Optional<Clan> clanId = clanDao.findById(clid);
		if (clanId.isPresent()) {

			return clanDao.findById(clid).get();
		}
		return null;
	}

	@Override
	public List<Clan> izlistajClanove() {
		return (List<Clan>) clanDao.findAll();

	}

	@Override
	public Clan unesiCl(ClanUnosDto cld) {
		Clan clan = new Clan();
		Optional<Clan> clanEmail = clanDao.findByEmail(cld.getEmail());
		if (!clanEmail.isPresent()) {

			Optional<Pretplata> pretplataf = pretplataDao.findById(cld.getPretplataId());
			if (pretplataf.isPresent()) {
				clan.setIme(cld.getIme());
				clan.setPrezime(cld.getPrezime());
				clan.setEmail(cld.getEmail());
				clan.setPretplata(pretplataf.get());

				return clanDao.save(clan);
			} else
				return null;
		}
		return null;

	}

	@Override
	public Clan promeniClan(ClanUnosDto clt) {
		Optional<Clan> clan = clanDao.findById(clt.getId());
		Optional<Pretplata> pretplataf = pretplataDao.findById(clt.getId());

		if (pretplataf.isPresent() && clan.isPresent()) {
			clan.get().setId(clt.getId());
			clan.get().setIme(clt.getIme());
			clan.get().setPrezime(clt.getPrezime());
			clan.get().setEmail(clt.getEmail());
			clan.get().setPretplata(pretplataf.get());

			return clanDao.save(clan.get());
		} else
			return null;

	}

	@Override
	public String toString() {
		return "ClanServis [pretplataDao=" + pretplataDao + ", clanDao=" + clanDao + "]";
	}

	@Override
	public ClanDugDto tekucaDugovanja(Integer id) {
		Double dugovanje = 0.00;
		Double mesecnapretplata = 1500.00;
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DATE, 1);
		Date pocetak = calendar.getTime();
		Date kraj = new Date();
		
		

		List<Termin> termini = terminDao.findByPocetakBetween(pocetak, kraj);
		Optional<Clan> clanId = clanDao.findById(id);
		if (clanId.isPresent()) {
			Clan clan = new Clan();
			clan = clanDao.findById(id).get();

			for (int i = 0; i < termini.size(); i++) {
				if (termini.get(i).getClan().getEmail().equals(clan.getEmail())) {
					dugovanje = dugovanje + termini.get(i).getCena();
				}
			}
			if (clan.getPretplata().getCena().equals(0)) {
				dugovanje = dugovanje + mesecnapretplata;
			}
			;
			ClanDugDto clanDugDto = new ClanDugDto();
			clanDugDto.setIme(clan.getIme());
			clanDugDto.setEmail(clan.getEmail());
			clanDugDto.setDugovanje(dugovanje);

			return clanDugDto;
		}
		return null;
	}

}
