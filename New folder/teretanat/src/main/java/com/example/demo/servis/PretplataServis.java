package com.example.demo.servis;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.PretplataDao;
import com.example.demo.domain.Pretplata;

@Service
public class PretplataServis {

	@Autowired
	private PretplataDao pretplataDao;

	public Pretplata nadjiPretpatu(Integer id) {

		return pretplataDao.findById(id).get();
	}
}
