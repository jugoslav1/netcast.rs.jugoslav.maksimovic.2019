package com.example.demo.domain.dto;

import java.util.ArrayList;
import java.util.List;

import com.example.demo.domain.Termin;

public class MailDto {
	private Integer id;
	private String ime;
	private String prezime;
	private Double dugovanje;
	private List<Termin> termini = new ArrayList<Termin>();
	public MailDto(Integer id, String ime, String prezime, Double dugovanje, List<Termin> termini) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.dugovanje = dugovanje;
		this.termini = termini;
	}
	public MailDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public Double getDugovanje() {
		return dugovanje;
	}
	public void setDugovanje(Double dugovanje) {
		this.dugovanje = dugovanje;
	}
	public List<Termin> getTermini() {
		return termini;
	}
	public void setTermini(List<Termin> termini) {
		this.termini = termini;
	}
	@Override
	public String toString() {
		return "MailDto [id=" + id + ", ime=" + ime + ", prezime=" + prezime + ", dugovanje=" + dugovanje + ", termini="
				+ termini + "]";
	}
	
	
}
