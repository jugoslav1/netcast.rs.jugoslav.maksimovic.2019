package com.example.demo.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Clan;
import com.example.demo.domain.Grupa;
import com.example.demo.domain.dto.ClanUnosDto;
import com.example.demo.servis.intF.GrupaIF;

@RestController
@RequestMapping("/grupa")
public class GrupaRest {

	@Autowired
	private GrupaIF grupaIF;

	@PostMapping("/registracija")
	public Grupa unesiGrupa(@RequestBody Grupa grupa) {

		return grupaIF.unesiGrupa(grupa);

	}

	@GetMapping("/poid/{id}")
	public Grupa poId(@PathVariable Integer id) {
		return grupaIF.nadjiPoIdGrupa(id);

	}

}
