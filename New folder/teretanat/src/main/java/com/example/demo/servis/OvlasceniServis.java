package com.example.demo.servis;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.dao.OvlasceniDao;
import com.example.demo.domain.Clan;
import com.example.demo.domain.Ovlasceni;
import com.example.demo.domain.dto.OvlasceniLogDto;
import com.example.demo.servis.intF.FilterIF;
import com.example.demo.servis.intF.OvlasIF;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class OvlasceniServis implements OvlasIF, FilterIF {

	public static final String KEY = "5445";
	@Autowired
	private OvlasceniDao ovlasceniDao;

	@Override
	public Ovlasceni findByEmailAndLozinka(String email, String lozinka) {
		Ovlasceni ovlasceni = ovlasceniDao.findByEmailAndLozinka(email, lozinka);
		return ovlasceni;
	}

	@Override
	public Ovlasceni findByEmail(String email) {
		Ovlasceni ovlascen = ovlasceniDao.findByEmail(email);
		return ovlascen;
	}

	@Override
	public String napraviToken(OvlasceniLogDto old) {
		Ovlasceni ovlascen = ovlasceniDao.findByEmailAndLozinka(old.getEmail(), old.getLozinka());

		if (ovlascen != null) {
			System.out.println("uso");
			String jwtsic = Jwts.builder().setIssuer(ovlascen.getLozinka()).setSubject(ovlascen.getEmail())
					.claim("ime", ovlascen.getIme() + "prezime" + ovlascen.getPrezime())
					.setIssuedAt(Date.from(Instant.ofEpochSecond(1466796822l))).signWith(SignatureAlgorithm.HS256, KEY)
					.compact();
			return jwtsic;
		}
		return null;
	}

	@Override
	public boolean proveriToken(String token) {
		String ovlascenEmail = this.emailPoTokenu(token);
		return (ovlascenEmail != null);
	}

	@Override
	public String emailPoTokenu(@RequestParam String token) {
		Jws<Claims> jws = Jwts.parser().setSigningKey(KEY).parseClaimsJws(token);
		return jws.getBody().getSubject();
	}

	@Override
	public Ovlasceni unesiOvl(Ovlasceni ovl) {
		Ovlasceni ovlascenEmail = ovlasceniDao.findByEmail(ovl.getEmail());
		if (ovlasceniDao.findByEmail(ovl.getEmail()).equals(null)) {
			return ovlasceniDao.save(ovl);
		}
		return null;
	}

	@Override
	public List<Ovlasceni> izlistajOvlascene() {

		return (List<Ovlasceni>) ovlasceniDao.findAll();
	}

}
