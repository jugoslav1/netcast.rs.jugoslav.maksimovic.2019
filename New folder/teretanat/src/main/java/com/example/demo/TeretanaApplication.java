package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.example.demo.domain.Pretplata;

@SpringBootApplication
//@EnableJpaAuditing
@EnableScheduling
public class TeretanaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeretanaApplication.class, args);
	
	}

}
