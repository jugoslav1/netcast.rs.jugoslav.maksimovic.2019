package com.example.demo.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.domain.Trener;

@Repository
public interface TrenerDao extends CrudRepository<Trener, Integer> {
}
