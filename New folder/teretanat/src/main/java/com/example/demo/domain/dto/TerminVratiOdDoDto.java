package com.example.demo.domain.dto;

import java.util.Date;

public class TerminVratiOdDoDto {

	private Integer id;
	private String imeClan;
	private String ImeTrener;
	private Date pocetak;
	private Date kraj;
	private Double cena;

	public TerminVratiOdDoDto(Integer id, String imeClan, String imeTrener, Date pocetak, Date kraj, Double cena) {
		super();
		this.id = id;
		this.imeClan = imeClan;
		ImeTrener = imeTrener;
		this.pocetak = pocetak;
		this.kraj = kraj;
		this.cena = cena;
	}

	public TerminVratiOdDoDto() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getImeClan() {
		return imeClan;
	}

	public void setImeClan(String imeClan) {
		this.imeClan = imeClan;
	}

	public String getImeTrener() {
		return ImeTrener;
	}

	public void setImeTrener(String imeTrener) {
		ImeTrener = imeTrener;
	}

	public Date getPocetak() {
		return pocetak;
	}

	public void setPocetak(Date pocetak) {
		this.pocetak = pocetak;
	}

	public Date getKraj() {
		return kraj;
	}

	public void setKraj(Date kraj) {
		this.kraj = kraj;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	@Override
	public String toString() {
		return "TerminVratiOdDoDto [id=" + id + ", imeClan=" + imeClan + ", ImeTrener=" + ImeTrener + ", pocetak="
				+ pocetak + ", kraj=" + kraj + ", cena=" + cena + "]";
	}

}
