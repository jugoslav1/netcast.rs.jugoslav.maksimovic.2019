package com.example.demo.domain.dto;

public class ClanDugDto {
	public String ime;
	public String email;
	public Double dugovanje;

	public ClanDugDto(String ime, String email, Double dugovanje) {
		super();
		this.ime = ime;
		this.email = email;
		this.dugovanje = dugovanje;
	}

	public ClanDugDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Double getDugovanje() {
		return dugovanje;
	}

	public void setDugovanje(Double dugovanje) {
		this.dugovanje = dugovanje;
	}

}
