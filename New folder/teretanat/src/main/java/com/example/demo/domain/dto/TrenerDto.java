package com.example.demo.domain.dto;

public class TrenerDto {
	private String ime;
	private Double cena;

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	private TrenerDto(String ime, Double cena) {
		super();
		this.ime = ime;
		this.cena = cena;
	}

	public TrenerDto() {

	}

	@Override
	public String toString() {
		return "TrenerDto [ime=" + ime + ", cena=" + cena + "]";
	}

}
