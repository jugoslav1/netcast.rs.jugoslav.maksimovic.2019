package com.example.demo.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "clan")
public class Clan {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String ime;
	private String prezime;

	private String email;

	@JsonIgnore
	@JoinColumn(name = "pretplata_id")
	@ManyToOne
	private Pretplata pretplata;
	@JsonIgnore
	@JoinColumn(name = "grupa_id")
	@ManyToOne
	private Grupa grupa;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "clan")
	private List<Termin> termini = new ArrayList<Termin>();

	public Pretplata getPretplata() {
		return pretplata;
	}

	public void setPretplata(Pretplata pretplata) {
		this.pretplata = pretplata;
	}

	public Clan() {

	}

	public Grupa getGrupa() {
		return grupa;
	}

	public void setGrupa(Grupa grupa) {
		this.grupa = grupa;
	}

	public Clan(Integer id, String ime, String prezime, String email, Pretplata pretplata, Grupa grupa,
			List<Termin> termini) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.email = email;
		this.pretplata = pretplata;
		this.grupa = grupa;
		this.termini = termini;
	}

	public List<Termin> getTermini() {
		return termini;
	}

	public void setTermini(List<Termin> termini) {
		this.termini = termini;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Clan [id=" + id + ", ime=" + ime + ", prezime=" + prezime + ", email=" + email + " , termini=" + termini + "]";
	}

}
