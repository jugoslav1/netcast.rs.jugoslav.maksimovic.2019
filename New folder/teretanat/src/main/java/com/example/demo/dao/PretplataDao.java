package com.example.demo.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.domain.Pretplata;

@Repository
@Transactional
public interface PretplataDao extends CrudRepository<Pretplata, Integer> {

}
