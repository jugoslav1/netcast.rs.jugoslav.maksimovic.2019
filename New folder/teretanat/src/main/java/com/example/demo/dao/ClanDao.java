package com.example.demo.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.domain.Clan;
import com.example.demo.domain.Termin;

@Repository
public interface ClanDao extends CrudRepository<Clan, Integer> {
	public Optional<Clan> findByEmail(String em);

	
}
