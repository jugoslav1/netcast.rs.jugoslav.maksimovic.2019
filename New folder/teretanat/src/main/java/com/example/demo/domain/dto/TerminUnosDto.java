package com.example.demo.domain.dto;

import java.sql.Date;

public class TerminUnosDto {

	private Integer id;
	private Date pocetak;
	private Date kraj;
	private Integer clanId;
	private Integer trenerId;

	public TerminUnosDto(Integer id, Date pocetak, Date kraj, Integer clanId, Integer trenerId) {
		super();
		this.id = id;
		this.pocetak = pocetak;
		this.kraj = kraj;
		this.clanId = clanId;
		this.trenerId = trenerId;
	}

	public TerminUnosDto() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getPocetak() {
		return pocetak;
	}

	public void setPocetak(Date pocetak) {
		this.pocetak = pocetak;
	}

	public Date getKraj() {
		return kraj;
	}

	public void setKraj(Date kraj) {
		this.kraj = kraj;
	}

	public Integer getClanId() {
		return clanId;
	}

	public void setClanId(Integer clanId) {
		this.clanId = clanId;
	}

	public Integer getTrenerId() {
		return trenerId;
	}

	public void setTrenerId(Integer trenerId) {
		this.trenerId = trenerId;
	}

	@Override
	public String toString() {
		return "TerminUnosDto [id=" + id + ", pocetak=" + pocetak + ", kraj=" + kraj + ", clanId=" + clanId
				+ ", trenerId=" + trenerId + "]";
	}

}
