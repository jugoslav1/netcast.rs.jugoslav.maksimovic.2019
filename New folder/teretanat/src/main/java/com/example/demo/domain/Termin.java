package com.example.demo.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "termin")
public class Termin {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private Date pocetak;

	private Date kraj;

	private Double cena;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "trener_id")
	private Trener trener;

	@JsonIgnore
	@JoinColumn(name = "clan_id")
	@ManyToOne
	private Clan clan;

	@JsonIgnore
	@JoinColumn(name = "ovlasceni_id")
	@ManyToOne
	private Ovlasceni ovlasceni;

	public Termin(Integer id, Date pocetak, Date kraj, Double cena, Trener trener, Clan clan, Ovlasceni ovlasceni) {
		super();
		this.id = id;
		this.pocetak = pocetak;
		this.kraj = kraj;
		this.cena = cena;
		this.trener = trener;
		this.clan = clan;
		this.ovlasceni = ovlasceni;
	}

	public Ovlasceni getOvlasceni() {
		return ovlasceni;
	}

	public void setOvlasceni(Ovlasceni ovlasceni) {
		this.ovlasceni = ovlasceni;
	}

	public Termin() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getPocetak() {
		return pocetak;
	}

	public void setPocetak(@NotBlank Date pocetak) {
		this.pocetak = pocetak;
	}

	public Date getKraj() {
		return kraj;
	}

	public void setKraj(@NotBlank Date kraj) {
		this.kraj = kraj;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public Trener getTrener() {
		return trener;
	}

	public void setTrener(Trener trener) {
		this.trener = trener;
	}

	public Clan getClan() {
		return clan;
	}

	public void setClan(Clan clan) {
		this.clan = clan;
	}

	@Override
	public String toString() {
		return "Termin [id=" + id + ", pocetak=" + pocetak + ", kraj=" + kraj + ", cena=" + cena + ", trener=" + trener
				+ ", clan=" + clan + "]";
	}

}
