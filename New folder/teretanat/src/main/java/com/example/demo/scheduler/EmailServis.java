package com.example.demo.scheduler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ClanDao;
import com.example.demo.dao.TerminDao;
import com.example.demo.domain.Clan;
import com.example.demo.domain.Termin;
import com.example.demo.domain.dto.ClanDugDto;
import com.example.demo.domain.dto.MailDto;
import com.example.demo.servis.ClanServis;

@Service
public class EmailServis implements EmailSendIF {
	@Autowired
	private JavaMailSender javaMailSender;
	@Autowired
	private ClanDao clanDao;
	@Autowired
	private TerminDao terminDao;
	private MailDto mailDto;

	public EmailServis(JavaMailSender javaMailSender, ClanDao clanDao, TerminDao terminDao, ClanServis clanServis) {
		super();
		this.javaMailSender = javaMailSender;
		this.clanDao = clanDao;
		this.terminDao = terminDao;

	}

	public EmailServis() {

	}

	@Override
	public void posaljiEmail() throws MailException {

		List<Clan> clanovi = (List<Clan>) clanDao.findAll();
		for (int i = 0; i <clanovi.size()-1; i++) {
			SimpleMailMessage mail = new SimpleMailMessage();
			mail.setTo(clanovi.get(i).getEmail());
			mail.setFrom("92f7f5b25e23da.mailtrap.io");
			mail.setSubject("mesecni izvestaj");
			System.out.println("ffffffffff"+clanovi.get(i).getIme());
			//mail.setText(i+"brebre  "+this.nadjiPoClanuTermineOdDoIDugovanje(clanovi.get(i).getId()));
			mail.setText("hhh"+clanovi.get(i).getTermini().get(1).toString());
			javaMailSender.send(mail);
		}
	}

	public String nadjiPoClanuTermineOdDoIDugovanje(Integer id) {
		Double dugovanje = 0.00;
		Double mesecnapretplata = 1500.00;
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DATE, 1);
		Date od = calendar.getTime();
		Date dok = new Date();

		System.out.println(dok);

		Optional<Clan> clanId = clanDao.findById(id);
		System.out.println(clanId.get().getEmail());

		/*if (clanId.isPresent()) {
			mailDto.setTermini(clanId.get().getTermini());
			for(int i=0;i<mailDto.getTermini().size();i++)
			{
				System.out.println( mailDto.getTermini().get(i).getCena());
				
			dugovanje = dugovanje + mailDto.getTermini().get(i).getCena();
			}*/
			if(clanId.get().getPretplata().equals(1)) dugovanje = dugovanje + mesecnapretplata;
			
			mailDto.setDugovanje(dugovanje);
			mailDto.setIme(clanId.get().getIme());
			mailDto.setPrezime(clanId.get().getPrezime());
			mailDto.setDugovanje(dugovanje);
			
			return ("tobato"+clanId.get().getEmail());

		/*}
		return "tobato";*/

	}
}
