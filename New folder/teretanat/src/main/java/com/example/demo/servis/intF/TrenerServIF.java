package com.example.demo.servis.intF;

import java.util.List;

import com.example.demo.domain.Trener;
import com.example.demo.domain.dto.TrenerDto;

public interface TrenerServIF {

	public Trener unesiTrenera(Trener tr);

	public TrenerDto unesiNazadDto(Trener tr);

	public List<Trener> izlistajTrenere();

	public Trener nadjiPoIdTrenera(Integer id);
}
