package com.example.demo.servis.intF;

import org.springframework.web.bind.annotation.RequestParam;

public interface FilterIF {

	public boolean proveriToken(String token);

	public String emailPoTokenu(@RequestParam String token);

}
