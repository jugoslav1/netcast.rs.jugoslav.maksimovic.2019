package com.example.demo.servis.intF;

import java.util.List;

import com.example.demo.domain.Clan;
import com.example.demo.domain.Pretplata;
import com.example.demo.domain.Termin;
import com.example.demo.domain.Trener;
import com.example.demo.domain.dto.TerminOdDoDto;
import com.example.demo.domain.dto.TerminUnosDto;
import com.example.demo.domain.dto.TerminVratiOdDoDto;

public interface TerminServIF {

	public Termin unesiTer(TerminUnosDto ted, String email);

	public List<Termin> pocetakOdDo1(TerminOdDoDto to);

	public List<TerminVratiOdDoDto> pocetakOdDo(TerminOdDoDto to);

}