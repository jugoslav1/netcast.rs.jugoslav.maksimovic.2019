package com.example.demo.vremeDomain;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="korisnik")
public class Korisnik {
@Id
private String id;
private String ime;
private String prezime;
private String email;

private String lozinka;
private List<Pretplata> pretplata;
public Korisnik(String id, String ime, String prezime, String email, String lozinka, List<Pretplata> pretplata) {
	super();
	this.id = id;
	this.ime = ime;
	this.prezime = prezime;
	this.email = email;
	this.lozinka = lozinka;
	this.pretplata = pretplata;
}
public Korisnik() {
	super();
	// TODO Auto-generated constructor stub
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getIme() {
	return ime;
}
public void setIme(String ime) {
	this.ime = ime;
}
public String getPrezime() {
	return prezime;
}
public void setPrezime(String prezime) {
	this.prezime = prezime;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getLozinka() {
	return lozinka;
}
public void setLozinka(String lozinka) {
	this.lozinka = lozinka;
}
public List<Pretplata> getPretplata() {
	return pretplata;
}
public void setPretplata(List<Pretplata> pretplata) {
	this.pretplata = pretplata;
}
@Override
public String toString() {
	return "Korisnik [id=" + id + ", ime=" + ime + ", prezime=" + prezime + ", email=" + email + ", lozinka=" + lozinka
			+ ", pretplata=" + pretplata + "]";
}

}
