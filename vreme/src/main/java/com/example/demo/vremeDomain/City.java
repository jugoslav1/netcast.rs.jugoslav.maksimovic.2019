package com.example.demo.vremeDomain;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "city")
public class City {
	
	@Id
	private String id;
	
	private String name;

	private Coordinates coord;
	
	private Sys sys;
	
	private Weather[] weather;
	
	private Main main;
	
	private Wind wind;
	
	private Clouds clouds;
	
	private Long dt;	
	
	private Long visibility;
	
	private Date lastWeatherInfo;
	
	private List<SavedTemperatures> savedTemperatures;
	
	private Integer cod;
	

	public City() {
		super();
	}

	

	



	
	public City(String id, String name, Coordinates coord, Sys sys, Weather[] weather, Main main, Wind wind,
			Clouds clouds, Long dt, Long visibility, Date lastWeatherInfo, List<SavedTemperatures> savedTemperatures,
			Integer cod) {
		super();
		this.id = id;
		this.name = name;
		this.coord = coord;
		this.sys = sys;
		this.weather = weather;
		this.main = main;
		this.wind = wind;
		this.clouds = clouds;
		this.dt = dt;
		this.visibility = visibility;
		this.lastWeatherInfo = lastWeatherInfo;
		this.savedTemperatures = savedTemperatures;
		this.cod = cod;
	}








	public Long getVisibility() {
		return visibility;
	}




	public void setVisibility(Long visibility) {
		this.visibility = visibility;
	}




	public Coordinates getCoord() {
		return coord;
	}

	public void setCoord(Coordinates coor) {
		this.coord = coor;
	}

	public Sys getSys() {
		return sys;
	}

	public void setSys(Sys sys) {
		this.sys = sys;
	}

	public Weather[] getWeather() {
		return weather;
	}

	public void setWeather(Weather[] weather) {
		this.weather = weather;
	}

	public Main getMain() {
		return main;
	}

	public void setMain(Main main) {
		this.main = main;
	}

	public Wind getWind() {
		return wind;
	}

	public void setWind(Wind wind) {
		this.wind = wind;
	}

	public Clouds getClouds() {
		return clouds;
	}

	public void setClouds(Clouds clouds) {
		this.clouds = clouds;
	}

	public Long getDt() {
		return dt;
	}

	public void setDt(Long dt) {
		this.dt = dt;
	}

	
	public Integer getCod() {
		return cod;
	}








	public void setCod(Integer cod) {
		this.cod = cod;
	}








	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public Date getLastWeatherInfo() {
		return lastWeatherInfo;
	}

	public void setLastWeatherInfo(Date lastWeatherInfo) {
		this.lastWeatherInfo = lastWeatherInfo;
	}
	
	public List<SavedTemperatures> getSavedTemperatures() {
		return savedTemperatures;
	}

	public void setSavedTemperatures(List<SavedTemperatures> savedTemperatures) {
		this.savedTemperatures = savedTemperatures;
	}

	@Override
	public String toString() {
		return "City [coord=" + coord + ", sys=" + sys + ", weather=" + Arrays.toString(weather) + ", main=" + main
				+ ", wind=" + wind + ", clouds=" + clouds + ", dt=" + dt + ", id=" + id + ", name=" + name
				+ ", lastWeatherInfo=" + lastWeatherInfo + ", savedTemperatures=" + savedTemperatures + "]";
	}

	
	


	
	
	
	
	
	
}
