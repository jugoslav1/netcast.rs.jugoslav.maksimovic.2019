package com.example.demo.vremeDomain;

public class Clouds {

	private Integer all;

	public Clouds() {
		super();
	}

	public Clouds(Integer all) {
		super();
		this.all = all;
	}

	public Integer getAll() {
		return all;
	}

	public void setAll(Integer all) {
		this.all = all;
	}

	@Override
	public String toString() {
		return "Clouds [all=" + all + "]";
	}
	
	
	
}
