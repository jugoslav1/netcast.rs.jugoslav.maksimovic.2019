package com.example.demo.vremeDomain.vremeDto;

public class KorisnikLogDto {
	
private String email;
private String lozinka;
public KorisnikLogDto(String email, String lozinka) {
	super();
	this.email = email;
	this.lozinka = lozinka;
}
public KorisnikLogDto() {
	super();
	// TODO Auto-generated constructor stub
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getLozinka() {
	return lozinka;
}
public void setLozinka(String lozinka) {
	this.lozinka = lozinka;
}

}
