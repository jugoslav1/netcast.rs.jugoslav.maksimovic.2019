package com.example.demo.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.vremeBezbednost.FilterIF;
import com.example.demo.vremeDomain.Korisnik;
import com.example.demo.vremeDomain.vremeDto.KorisnikPretplataDto;
import com.example.demo.vremeDomain.vremeDto.KorisnikUnosDto;
import com.example.demo.vremeIF.KorisnikIF;
import com.example.demo.vremeDomain.vremeDto.KorisnikLogDto;
@RestController
@RequestMapping("/korisnici")
public class KorisnikRest {
     @Autowired
     KorisnikIF korisnikIF;
     @Autowired
     FilterIF filterIF;
     
     @PostMapping("/registracija")
     public KorisnikUnosDto unesiKorisnika(@RequestBody KorisnikUnosDto kud)
     {
    	 return korisnikIF.unesiKorisnika(kud);
     }
     @PostMapping("/pretplata")
     public KorisnikPretplataDto unesiPretplatu(KorisnikPretplataDto kpd)
     {
    	 return unesiPretplatu(kpd);
    	 
     }
    
     @PostMapping("/logovanje")
 	public String napraviJwt(@RequestBody KorisnikLogDto kld) {
 		return filterIF.napraviToken(kld);
 	}
     
}
