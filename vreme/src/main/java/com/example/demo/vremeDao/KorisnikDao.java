package com.example.demo.vremeDao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.vremeDomain.Korisnik;

@Repository
public interface KorisnikDao extends MongoRepository<Korisnik, String> {
	
	
	public Korisnik findByEmail(String email);
	
	public Korisnik findByEmailAndLozinka(String email,String lozinka);
	
}	
	


