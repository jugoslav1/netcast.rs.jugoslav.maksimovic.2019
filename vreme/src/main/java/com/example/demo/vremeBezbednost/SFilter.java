package com.example.demo.vremeBezbednost;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



//@Component
public class SFilter implements Filter {
	@Autowired
	private FilterIF filterIF;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		String token = httpRequest.getHeader("token");

		if (httpRequest.getRequestURI().contains("/korisnici/logovanje")||httpRequest.getRequestURI().contains("/korisnici/registracija")) {
			chain.doFilter(request, response);
			return;
		}

		
		
		if (filterIF.proveriToken(token)) {
			String ovlasceniEmail = filterIF.emailPoTokenu(token);

			if (ovlasceniEmail != null) {
				httpRequest.setAttribute("OVLASCENI_MAIL", ovlasceniEmail);
				chain.doFilter(request, response);

				return;
			} else
				httpResponse.setStatus(401);

		}
		httpResponse.setStatus(401);
		;
	}
}