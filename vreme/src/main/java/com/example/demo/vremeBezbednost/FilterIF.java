package com.example.demo.vremeBezbednost;

import org.springframework.web.bind.annotation.RequestParam;


import com.example.demo.vremeDomain.Korisnik;
import com.example.demo.vremeDomain.vremeDto.KorisnikLogDto;

public interface FilterIF {

	public boolean proveriToken(String token);

	public String emailPoTokenu(@RequestParam String token);
	public Korisnik findByEmail(String email);
	public String napraviToken(KorisnikLogDto old);
	public Korisnik findByEmailAndLozinka(String email, String lozinka);
}
