package com.example.demo.vremeBezbednost;

import java.time.Instant;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.vremeDao.KorisnikDao;
import com.example.demo.vremeDomain.Korisnik;
import com.example.demo.vremeDomain.vremeDto.KorisnikLogDto;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class FilterServis implements  FilterIF{
	
	
	
	public static final String KEY = "5445";
	@Autowired
	private KorisnikDao korisnikDao;

	@Override
	public Korisnik findByEmailAndLozinka(String email, String lozinka) {
		Korisnik korisnik = korisnikDao.findByEmailAndLozinka(email, lozinka);
		return korisnik;
	}

	@Override
	public Korisnik findByEmail(String email) {
		Korisnik korisnik = korisnikDao.findByEmail(email);
		return korisnik;
	}

	@Override
	public String napraviToken(KorisnikLogDto old) {
		Korisnik korisnik = korisnikDao.findByEmailAndLozinka(old.getEmail(), old.getLozinka());

		if (korisnik != null) {
			System.out.println("uso");
			String jwtsic = Jwts.builder().setIssuer(korisnik.getLozinka()).setSubject(korisnik.getEmail())
					.claim("ime", korisnik.getIme() + "prezime" +korisnik.getPrezime())
					.setIssuedAt(Date.from(Instant.ofEpochSecond(1466796822l))).signWith(SignatureAlgorithm.HS256, KEY)
					.compact();
			return jwtsic;
		}
		return null;
	}

	@Override
	public boolean proveriToken(String token) {
		String korisnikEmail = this.emailPoTokenu(token);
		return (korisnikEmail != null);
	}

	@Override
	public String emailPoTokenu(@RequestParam String token) {
		Jws<Claims> jws = Jwts.parser().setSigningKey(KEY).parseClaimsJws(token);
		return jws.getBody().getSubject();
	}


}
