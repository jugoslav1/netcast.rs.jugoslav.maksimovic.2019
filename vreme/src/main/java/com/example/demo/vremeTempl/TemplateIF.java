package com.example.demo.vremeTempl;

import java.util.List;

import com.example.demo.vremeDomain.City;



public interface TemplateIF {

	public List<City> getAllCities(List<City> cities);
	
}
