package com.example.demo.vremeTempl;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.demo.properties.WeatherProperty;
import com.example.demo.vremeDomain.City;





@Service
public class Template implements TemplateIF {

	


		
		@Override
		public List<City> getAllCities(List<City> cities) {

			RestTemplate template = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();	
			List<City> pdatedCities = new ArrayList<>();
			for(City c : cities) {
			headers.set(WeatherProperty.keyName,WeatherProperty.keyValue);
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			String url = WeatherProperty.url+c.getName();
			HttpEntity<String> entity = new HttpEntity<String>(headers);
			ResponseEntity<City> response = template.exchange(url, HttpMethod.GET, entity, City.class);
			City city = response.getBody();
			pdatedCities.add(city);
			
			}
			return pdatedCities;
		}

	
	/*	@Scheduled(fixedRate=20000)
		public void getAllCities() {

			RestTemplate template = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();	
			
			 
			headers.set(WeatherProperty.keyName,WeatherProperty.keyValue);
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			String url = WeatherProperty.url+"Budapest";
			HttpEntity<String> entity = new HttpEntity<String>(headers);
			ResponseEntity<City> response = template.exchange(url, HttpMethod.GET, entity, City.class);
			City city = response.getBody();
			
			System.out.println(city);
			citydao.save(city);
			
			
		}*/
		
}
