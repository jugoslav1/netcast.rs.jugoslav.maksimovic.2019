package com.example.demo.vremeIF;



import com.example.demo.vremeDomain.Korisnik;
import com.example.demo.vremeDomain.vremeDto.KorisnikPretplataDto;
import com.example.demo.vremeDomain.vremeDto.KorisnikUnosDto;

public interface KorisnikIF {
public KorisnikUnosDto unesiKorisnika(KorisnikUnosDto kud);
public KorisnikPretplataDto unesiPretplatu(KorisnikPretplataDto kpd);

}
